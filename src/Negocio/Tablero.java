/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Ficha;

/**
 *
 * @author madar
 */
public class Tablero {

    private Ficha[][] myTablero;
    private boolean direccion;  //true si el peón se mueve de arriba hacia abajo, o false en caso contrario
    //Adicione los atributos que considere necesarios para el correcto fucnioanmiento de su aplicación, si y solo si , no violen ninguna regla en POO

    public Tablero() {
    }

    /**
     * Constructor para iniciar el juego
     *
     * @param i_alfil Posición de la fila para el alfil
     * @param j_alfil Posición de la columna para el alfil
     * @param i_peon Posición de la fila para el peon
     * @param j_peon Posición de la columna para el alfil
     * @param dirPeon true si el peón se mueve de arriba hacia abajo, o false en
     * caso contrario
     */
    public Tablero(int i_alfil, int j_alfil, int i_peon, int j_peon, boolean dirPeon) {
        myTablero = new Ficha[8][8];
        myTablero[i_alfil][j_alfil] = new Ficha("alfil");
        myTablero[i_peon][j_peon] = new Ficha("peon");
        direccion = dirPeon;
    }

    public int[] localizar() {
        int pos[] = new int[4]; //xalfil,yalfil,xpeon,ypeon
        for (int i = 0; i < 8; i++) {    //para recorrer el tablero
            for (int j = 0; j < 8; j++) {
                if (myTablero[i][j] == null) {
                    continue;
                }
                if (myTablero[i][j].getNombreFicha().equalsIgnoreCase("alfil")) {   //buscamos la posicion del alfil
                    pos[0] = i;
                    pos[1] = j;
                }
                if (myTablero[i][j].getNombreFicha().equalsIgnoreCase("peon")) {  //buscamos la posicion del peon
                    pos[2] = i;
                    pos[3] = j;
                }
            }
        }
        return pos;
    }

    public String getCamino(int ia, int ja, int ip, int jp) {  //metodo recursivo para hallar el camino
        if ((direccion && ip > 7) || (!direccion && ip < 0)) {
            return "";
        }
        String peon = " - P(" + ip + "," + jp + ")";
        String alfil = null;
        String rta = null;

        if (Math.abs(jp - ja) == Math.abs(ip - ia)) {  //miro que la casilla no este amenazada por el alfil
            if (!direccion) {   //subiendo
                if (Math.abs(jp - ja) == 0) { //caso donde el alfil esta adelante del peon
                    ia++;
                    ja += (ja == 0) ? 1 : -1; //valida que el alfil no este en la primera columna
                    alfil = " - A(" + (ia) + "," + (ja) + ")"; //coordenadas del alfil para el camino
                    rta = alfil;
                    ip++;
                } else if (jp > ja) {     //verifica que el alfil este a la izq del peon
                    if (ip < ia) {   //verifica que el alfil este mas abajo que el peon
                        if (ia < 7) { //verifica que el alfil no este en la ultima fila
                            ia++;   // con estas 2 lineas se baja el alfil una posicion abajo - der
                            ja++;
                            alfil = " - A(" + (ia) + "," + (ja) + ")"; //coordenadas del alfil para el camino
                            rta = alfil + peon; //coordenadas de ambas fichas
                        } else {  //alfil en ultima fila
                            if (ja == 0) { //alfil en 7,0
                                if (jp == (ja + 1)) {//que el peon este pegado al alfil
                                    ia -= 6;
                                    ja += 6;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta = alfil;
                                    ia++;
                                    ja++;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta += alfil + peon;
                                } else { //peon no esta junto al alfil
                                    ia--;
                                    ja++;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta = alfil;
                                    ia++;
                                    ja++;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta += alfil + peon;
                                }
                            } else { // alfil en ultima fila pero no primera columna
                                ja--;
                                ia--;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil + peon;
                            }
                        }
                    } else {// alfil mas arriba del peon                       

                        if (ja == 0) { //alfil en primera columna 
                            if (jp == (ja + 1)) {//que el peon este pegado al alfil en columnas
                                ia += 2;
                                ja += 2;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil;
                                ia++;
                                ja--;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta += alfil + peon;
                            } else { // peon no pegado al alfil en filas  
                                ia++;
                                ja++;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil;
                                ia++;
                                ja--;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta += alfil + peon;
                            }
                        } else { // alfil no esta en primera columna 
                            ja--;
                            ia++;
                            alfil = " - A(" + (ia) + "," + (ja) + ")";
                            rta = alfil + peon;
                        }

                    }
                } else {//alfil a la derecha del peon

                    if (ip < ia) {   //verifica que el alfil este mas abajo que el peon
                        if (ia < 7) { //verifica que el alfil no este en la ultima fila
                            ia++;   // con estas 2 lineas se baja el alfil una posicion abajo - izq
                            ja--;
                            alfil = " - A(" + (ia) + "," + (ja) + ")"; //coordenadas del alfil para el camino
                            rta = alfil + peon; //coordenadas de ambas fichas                            
                        } else {  //alfil en ultima fila
                            if (ja == 7) { //alfil en 7,7 
                                if (ja == (jp + 1)) {//que el peon este pegado al alfil
                                    ia -= 6;
                                    ja -= 6;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta = alfil;
                                    ia++;
                                    ja--;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta += alfil + peon;
                                } else { //peon no esta junto al alfil
                                    ia--;
                                    ja--;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta = alfil;
                                    ia++;
                                    ja--;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta += alfil + peon;
                                }
                            } else { // alfil en ultima fila pero no ultima columna
                                ja++;
                                ia--;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil + peon;
                            }
                        }
                    } else {// alfil mas arriba del peon                      
                        if (ja == 7) { //alfil en columna 7
                            if (ja == (jp + 1)) {//que el peon este pegado al alfil en columnas
                                ia += 2;
                                ja -= 2;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil;
                                ia++;
                                ja++;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta += alfil + peon;
                            } else { // peon no pegado al alfil en filas  
                                ia++;
                                ja--;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil;
                                ia++;
                                ja++;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta += alfil + peon;
                            }
                        } else { // alfil en cualquier fila pero no ultima columna
                            ja++;
                            ia++;
                            alfil = " - A(" + (ia) + "," + (ja) + ")";
                            rta = alfil + peon;
                        }
                    }
                }
            } else { //bajando 
                if (Math.abs(jp - ja) == 0) { //caso donde el peon este atras del alfil
                    ia--;
                    ja += (ja == 7) ? -1 : 1;//valida que no sea la ultima columna
                    alfil = " - A(" + (ia) + "," + (ja) + ")"; //coordenadas del alfil para el camino
                    rta = alfil;
                    ip--;
                } else if (jp > ja) {     //verifica que el alfil este a la izq del peon
                    if (ip < ia) {   //verifica que el alfil este mas abajo que el peon
                        if (ja == 1) { //alfil en columna 7
                            if (ja == (jp - 1)) {//que el peon este pegado al alfil en columnas
                                ia -= 2;
                                ja += 2;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil;
                                ia--;
                                ja--;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta += alfil + peon;
                            } else { // peon no pegado al alfil en filas  
                                ia--;
                                ja++;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil;
                                ia--;
                                ja--;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta += alfil + peon;
                            }
                        } else { // alfil no esta en primera columna
                            ja--;
                            ia--;
                            alfil = " - A(" + (ia) + "," + (ja) + ")";
                            rta = alfil + peon;
                        }
                    } else {// alfil mas arriba del peon                                        
                        if (ia > 0) { //verifica que el alfil no este en la primera fila
                            ia--;   // con estas 2 lineas se sube el alfil una posicion arriba - der
                            ja++;
                            alfil = " - A(" + (ia) + "," + (ja) + ")"; //coordenadas del alfil para el camino
                            rta = alfil + peon; //coordenadas de ambas fichas   

                        } else {  //alfil en primera fila
                            if (ja == 0) { //alfil en 0,0 
                                if (ja == (jp - 1)) {//que el peon este pegado al alfil
                                    ia += 6;
                                    ja += 6;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta = alfil;
                                    ia--;
                                    ja++;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta += alfil + peon;
                                } else { //peon no esta junto al alfil
                                    ia++;
                                    ja++;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta = alfil;
                                    ia--;
                                    ja++;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta += alfil + peon;
                                }
                            } else { // alfil en primera fila, pero no primera columna
                                ja--;
                                ia++;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil + peon;
                            }
                        }
                    }
                } else {//alfil a la derecha del peon
                    if (ip < ia) {   //verifica que el alfil este mas abajo que el peon
                        if (ja == 7) { //alfil en la columna 7 
                            if (jp == (ja - 1)) {//que el peon este pegado al alfil en columnas
                                ia -= 2;
                                ja -= 2;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil;
                                ia--;
                                ja++;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta += alfil + peon;
                            } else { // peon no pegado al alfil en filas  
                                ia--;
                                ja--;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil;
                                ia--;
                                ja++;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta += alfil + peon;
                            }
                        } else { // alfil no esta en la ultima columna
                            ja++;
                            ia--;
                            alfil = " - A(" + (ia) + "," + (ja) + ")";
                            rta = alfil + peon;
                        }
                    } else {// alfil mas arriba del peon                      
                        if (ia > 0) { //verifica que el alfil no este en la primera fila
                            ia--;   // con estas 2 lineas se sube el alfil una posicion arriba - izq
                            ja--;
                            alfil = " - A(" + (ia) + "," + (ja) + ")"; //coordenadas del alfil para el camino
                            rta = alfil + peon; //coordenadas de ambas fichas
                        } else {  //alfil en primer fila
                            if (ja == 7) { //alfil en 0,7
                                if (jp == (ja - 1)) {//que el peon este pegado al alfil en columnas
                                    ia += 6;
                                    ja -= 6;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta = alfil;
                                    ia--;
                                    ja--;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta += alfil + peon;
                                } else { //peon no esta junto al alfil
                                    ia++;
                                    ja--;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta = alfil;
                                    ia--;
                                    ja--;
                                    alfil = " - A(" + (ia) + "," + (ja) + ")";
                                    rta += alfil + peon;
                                }
                            } else { // alfil en ultima fila pero no primera columna
                                ja++;
                                ia++;
                                alfil = " - A(" + (ia) + "," + (ja) + ")";
                                rta = alfil + peon;
                            }
                        }
                    }
                }
            }
        } else { //no amenazado el peon
            rta = peon;
        }
        //System.out.println(rta);
        if (direccion) {
            return rta + getCamino(ia, ja, ip + 1, jp);
        } else {
            return rta + getCamino(ia, ja, ip - 1, jp);
        }
    }

    public String jugar() {
        //:)
        int posiciones[] = localizar();
        String rta = "A(" + posiciones[0] + "," + posiciones[1] + ")";
        rta += getCamino(posiciones[0], posiciones[1], posiciones[2], posiciones[3]);
        return rta;
    }
}
